.. include:: ../README.rst

.. toctree::
    :maxdepth: 1
    :caption: Getting Started

    getting-started/installation
    getting-started/licensing-notes

.. toctree::
    :maxdepth: 1
    :caption: User Guide

    user/coll
    user/gamma
    user/electronfactors
    user/dcm
    user/geometry

.. toctree::
    :maxdepth: 1
    :caption: Developer Guide

    developer/contributing
    developer/documentation
    developer/physical-design
    developer/agpl-benefits


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
