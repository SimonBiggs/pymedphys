Collimator Toolbox
==================

.. automodule:: pymedphys.coll
    :no-members:

MU Density
----------

.. automodule:: pymedphys._level2.collmudensity
    :no-members:

.. plot:: pyplots/mudensity.py
   :include-source:

.. plot:: pyplots/single_leaf.py
   :include-source:


API
---

.. autofunction:: pymedphys.coll.calc_mu_density

.. autofunction:: pymedphys.coll.calc_single_control_point

.. autofunction:: pymedphys.coll.single_mlc_pair

.. autofunction:: pymedphys.coll.get_grid

.. autofunction:: pymedphys.coll.find_relevant_control_points

.. autofunction:: pymedphys.coll.display_mu_density
