Dicom Toolbox
=============

.. automodule:: pymedphys.dcm
    :no-members:

API
---

.. _`pymedphys.geometry.cubify_cube_definition`: geometry.html#pymedphys.geometry.cubify_cube_definition

.. autofunction:: pymedphys.dcm.anonymise_dicom

.. autofunction:: pymedphys.dcm.extract_dose

.. autofunction:: pymedphys.dcm.extract_iec_patient_coords

.. autofunction:: pymedphys.dcm.extract_iec_fixed_coords

.. autofunction:: pymedphys.dcm.extract_dicom_patient_coords

.. autofunction:: pymedphys.dcm.load_dose_from_dicom

.. autofunction:: pymedphys.dcm.load_xyz_from_dicom

.. autofunction:: pymedphys.dcm.find_dose_within_structure

.. autofunction:: pymedphys.dcm.create_dvh

.. autofunction:: pymedphys.dcm.get_structure_aligned_cube
