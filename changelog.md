# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- A 'hello world' browser based, jupyter powered, GUI interface from [@SimonBiggs](https://gitlab.com/SimonBiggs).

### Changed
- nil

### Deprecated
- nil

### Removed
- nil

### Fixed
- nil

### Safety
- nil

### Security
- nil


## [0.5.1] - 2018-01-05
### Added
- Began keeping record of changes in `changelog.md`


[Unreleased]: https://gitlab.com/pymedphys/pymedphys/compare/v0.5.1...master
[0.5.1]: https://gitlab.com/pymedphys/pymedphys/compare/v0.4.3...v0.5.1
